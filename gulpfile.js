const del = require('del')
const { src, dest, series, watch } = require('gulp')
const babel = require('gulp-babel')
const concat = require('gulp-concat')
const plumber = require('gulp-plumber')
const replace = require('gulp-replace')
const packageJson = require("./package.json")

function buildJS (done) {
  return src('./src/js/**/*.js')
    .pipe(plumber())
    .pipe(concat('main.js'))
    .pipe(babel({
      presets: [
        ['@babel/env', {
          modules: false
        }]
      ]
    }))
    .pipe(dest('./dist'))
}

function buildCSS (done) {
  return src('./src/style.css').pipe(dest('./dist'))
}

function buildHTML (done) {
  return src('./src/*.html')
    .pipe(replace('{{VERSION}}', packageJson.version))
    .pipe(dest('./dist'))
}

function buildImage (done) {
  return src('./src/images/*.svg').pipe(dest('./dist/images'))
}

function clean (done) {
  return del('./dist/**/*')
}

let build = series(buildJS, buildHTML, buildCSS, buildImage)

exports.buildJS = buildJS
exports.clean = clean
exports.dist = series(clean, build)
exports.build = build
exports.default = build
exports.live = series(build, () => {
  watch("src/images/*.svg", buildImage)
  watch("src/js/*.js", buildJS)
  watch("src/*.html", buildHTML)
  watch("src/*.css", buildCSS)
})