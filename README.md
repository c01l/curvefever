# curvefever

This is a clone of the classy game "CurveFever 1.0".

The gameplay is extremly simple. Just pick two keys on a keyboard that turns you to the left or the right.
*The last player standing wins!*

## About this clone

The goal was to imitate the original Flash game as close as possible using HTML5.
Once the base game was implemented the features kept rolling in: New items, unlimited player counts, new worlds, everything should be configurable, ...

If you have a feature idea do not hesitate to create an issue or even submit a pull request.



