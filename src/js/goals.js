let goals = {
  points: {
    name: 'Target Points',
    description: 'The player who reaches a specific number of points wins the game',
    getDescription: function () {
      return 'Target: ' + this.config.targetPoints + ' points<br />' + (this.config.pointsAway > 0 ? 'Difference: ' + this.config.pointsAway : '')
    },
    getWinner: function (game) {
      let getPlayerWithMaxPoints = (players) => {
        let max = players[0]
        for (let i in players) {
          if (max.points < players[i].points) {
            max = players[i]
          }
        }
        return max
      }

      let winner = getPlayerWithMaxPoints(game.players)
      if (winner.points >= this.config.targetPoints) {
        console.log('Got win, winner: ', winner.name)
        if (this.config.pointsAway > 0) {
          let second = getPlayerWithMaxPoints(game.players.filter(p => p !== winner))
          if (second.points + this.config.pointsAway > winner.points) {
            return null
          }
        }
        return winner
      }
      return null
    },
    configSpec: {
      targetPoints: {
        title: 'Points',
        description: 'How many points are needed for winning?',
        type: 'number',
        default: 10
      },
      pointsAway: {
        title: 'Difference',
        description: 'How many points does the second player needs to be away from the first player?',
        type: 'number',
        default: 0
      }
    }
  },
  fixedRounds: {
    name: 'Fixed Rounds',
    description: 'A fixed number of rounds is played. After that a winner is determined.',
    getDescription: function (game) {
      return 'Round ' + game.roundCounter + '/' + this.config.roundCount
    },
    getWinner: function (game) {
      if (game.roundCounter <= this.config.roundCount) {
        return null
      }

      let getPlayerWithMaxPoints = (players) => {
        let max = players[0]
        for (let i in players) {
          if (max.points < players[i].points) {
            max = players[i]
          }
        }
        return max
      }

      return getPlayerWithMaxPoints(game.players)
    },
    configSpec: {
      roundCount: {
        title: 'Rounds',
        description: 'How many rounds should be played?',
        type: 'number',
        default: 10
      }
    }
  }
}

let defaultGoal = goals.points

function loadGoalFromParams () {
  let params = new URLSearchParams(window.localStorage.getItem('currentGameConfig'))
  let goalName = params.get('goal')

  let goal = { ...goals[goalName], config: {} }
  for (let i in goal.configSpec) {
    let value = params.get('goal.config.' + i)
    if (goal.configSpec[i].type === 'number') {
      value = parseFloat(value)
    }
    goal.config[i] = value
  }

  return goal
}

function instantiateGoalWithDefaults (goalClass) {
  let goal = { ...goalClass, config: {} }
  for (let i in goal.configSpec) {
    goal.config[i] = goal.configSpec[i].default
  }
  return goal
}