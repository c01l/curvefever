class UIManager {

  message = ''
  animation = {}
  isDebug = false
  lastTickTime = 0
  portal = loadImage('images/portal.svg')

  init (game) {
    this.game = game

    this.canvas = document.getElementById('game-screen')
    this.resizeCanvasToDisplaySize(this.canvas)
    this.g = this.canvas.getContext('2d')

    this.updateGoalDescription()

    if (this.game.getConfig().effect.display.showInSidecar) {
      this.effectUpdaterInterval = window.setInterval(() => this.updateEffectDisplay(), 50)
    } else {
      document.querySelector(".sidecar .effects").remove()
    }
  }

  updateEffectDisplay () {
    const createEffectUI = (name, color, percent) => {
      const effect = document.createElement('div')
      effect.classList.add('effect')
      effect.style.color = color
      const title = document.createElement('div')
      title.classList.add('effect-title')
      title.textContent = name
      effect.append(title)
      const bar = document.createElement('div')
      bar.classList.add('effect-bar')
      bar.style.width = (100 - 100 * percent) + '%'
      bar.textContent = ' '
      effect.append(bar)
      return effect
    }

    let nodes = []
    this.game.effects.forEach(we => {
      nodes.push(createEffectUI(we.name, 'yellow', we.durationCounter / we.duration))
    })
    this.game.players.forEach(p => {
      nodes.push(...p.effects.map(pe => createEffectUI(pe.name + ' (' + p.name + ')', p.color, pe.durationCounter / pe.duration)))
    })

    const effectList = document.getElementById('effect-list')
    if (effectList !== null) {
      if (nodes.length > 0) {
        effectList.innerHTML = ''
        nodes.forEach((n) => effectList.appendChild(n))
      } else {
        effectList.innerHTML = '<p>None active</p>'
      }
    }
  }

  setLastTickTime (time) {
    this.lastTickTime = time
  }

  setDebug (flag) {
    this.isDebug = flag
  }

  updateGoalDescription () {
    let goal = this.game.getGoal()
    document.getElementById('goal-description').innerHTML = goal.getDescription.call(goal, this.game)
  }

  setMessage (msg) {
    this.message = msg
    // TODO display message in the middle of the screen
  }

  drawLoop () {
    this.draw()
    window.requestAnimationFrame(() => this.drawLoop())
  }

  setGameState (text) {
    document.getElementById('game-state').innerText = text
  }

  draw () {
    this.drawBackground()

    const oldAlpha = this.g.globalAlpha
    if (this.game.cloudMode && this.game.gameState === GAMESTATE_RUNNING) {
      this.g.globalAlpha = 1 - clamp(animate(this.animation, 'worldcloud', 0, 1.25, 0.017), 0, 1)
    }

    this.g.drawImage(this.game.movementCanvas, 0, 0)
    this.drawWorld()
    this.drawPlayerHeads(this.game.players)
    this.drawItems(this.game.items)
    this.drawPortals(this.game.portals)

    if (this.game.cloudMode) {
      this.g.globalAlpha = oldAlpha
    }

    if (this.isDebug) {
      this.drawDebugInfo()
    }
  }

  drawDebugInfo () {
    this.g.fillText('Tick: ' + (1000 / this.lastTickTime) + 'ms', 15, 35)
  }

  drawItems (items) {
    let old = this.g.globalAlpha
    this.g.globalAlpha = old * 0.7
    const size = this.game.getConfig().item.size
    items.forEach(item => {
      this.g.drawImage(item.image, item.pos.x - size / 2, item.pos.y - size / 2, size, size)
    })
    this.g.globalAlpha = old
  }

  updateRanking () {
    this.renderPlayerPoints(this.game.players)
  }

  renderPlayerPoints (players) {
    let cont = document.getElementById('player-ranking')
    cont.innerHTML = ''

    players.sort((a, b) => b.points - a.points)

    players.filter(p => !p.bot).forEach(p => {
      cont.innerHTML += '<div style=\'color: ' + p.color + '\'><span class=\'name\'>' + p.name + '</span><span class=\'points\'>' + p.points + '</span></div>'
      cont.innerHTML += ''
    })
  }

  drawPlayerHeads (players) {
    const config = this.game.getConfig()
    players.forEach(p => {
      this.g.beginPath()
      if (p.turnMode === TURNMODE_NORMAL) {
        this.g.arc(p.pos.x, p.pos.y, p.size, 0, 2 * Math.PI, false)
      } else if (p.turnMode === TURNMODE_90DEG) {
        this.g.rect(p.pos.x - p.size, p.pos.y - p.size, 2 * p.size, 2 * p.size)
      }
      let livingPlayerHeadColor = config.color.playerHead
      if (p.controls.switched) {
        livingPlayerHeadColor = config.color.playerHeadSwitched
      }

      if (!p.collision.world) {
        livingPlayerHeadColor = setColorStringAlpha(livingPlayerHeadColor,
          animate(p.animation, 'headBounce', 0.3, 1, 0.01))
      }
      this.g.fillStyle = p.isDead ? config.color.playerHeadDead : livingPlayerHeadColor
      this.g.fill()

      if (this.game.gameState === GAMESTATE_START || this.game.gameState === GAMESTATE_PAUSED) {
        this.g.fillStyle = p.color
        this.g.font = '20px Comic Sans MS'
        this.g.fillText(p.name, p.pos.x + 5, p.pos.y)

        this.g.strokeStyle = p.color
        this.g.strokeWidth = 2
        this.g.beginPath()
        this.g.moveTo(p.pos.x, p.pos.y)
        const len = 5
        const sx = p.pos.x + Math.cos(p.direction) * p.size * len
        const sy = p.pos.y + Math.sin(p.direction) * p.size * len
        this.g.lineTo(sx, sy)
        const off_angle = Math.PI / 4
        this.g.lineTo(sx - Math.cos(p.direction + off_angle) * p.size * 2, sy - Math.sin(p.direction + off_angle) * p.size * 2)
        this.g.moveTo(sx, sy)
        this.g.lineTo(sx - Math.cos(p.direction - off_angle) * p.size * 2, sy - Math.sin(p.direction - off_angle) * p.size * 2)
        this.g.stroke()
      }

      if (this.isDebug) {
        let text = `
Collision:\n
- World: ${p.collision.world}\n
- Paths: ${p.collision.paths}\n
`
        this.g.fillText(text, p.pos.x + 5, p.pos.y)
      }
    })
  }

  drawBackground () {
    let w = this.canvas.width
    let h = this.canvas.height

    this.g.fillStyle = this.game.getConfig().color.background
    this.g.fillRect(0, 0, w, h)
  }

  drawWorld () {
    let old
    if (!this.game.collision.world) {
      old = this.g.globalAlpha
      this.g.globalAlpha = animate(this.animation, 'worldBounce', 0, 1, 0.01)
    }
    this.g.drawImage(this.game.worldCollisionMapCanvas, 0, 0)
    if (!this.game.collision.world) {
      this.g.globalAlpha = old
    }
  }

  drawPortals (portals) {
    const drawSinglePortal = (portalPoint) => {
      const portalSize = this.game.getConfig().effect.game.portal.size / 2

      const dx = Math.cos(portalPoint.angle) * portalSize
      const dy = Math.sin(portalPoint.angle) * portalSize

      const e1x = portalPoint.x + dx
      const e1y = portalPoint.y + dy
      const e2x = portalPoint.x - dx
      const e2y = portalPoint.y - dy

      this.g.beginPath()
      this.g.strokeWidth = 1
      this.g.moveTo(e1x, e1y)
      this.g.lineTo(e2x, e2y)
      this.g.stroke()

      this.g.beginPath()
      this.g.fillStyle = '#ff0000'
      this.g.arc(e1x, e1y, 3, 0, Math.PI * 2)
      this.g.fill()
      this.g.beginPath()
      this.g.fillStyle = '#0000ff'
      this.g.arc(e2x, e2y, 3, 0, Math.PI * 2)
      this.g.fill()
    }

    portals.forEach(p => {
      this.g.fillStyle = '#ffffff'
      this.g.strokeStyle = '#ffffff'

      drawSinglePortal(p.p1)
      drawSinglePortal(p.p2)

      // this.g.drawImageRotated(this.portal, p.x, p.y, p.angle)
    })
  }

  // Src: https://stackoverflow.com/questions/4938346/canvas-width-and-height-in-html5
  resizeCanvasToDisplaySize (canvas) {
    // look up the size the canvas is being displayed
    const width = canvas.clientWidth
    const height = canvas.clientHeight

    // If it's resolution does not match change it
    if (canvas.width !== width || canvas.height !== height) {
      canvas.width = width
      canvas.height = height
      return true
    }

    return false
  }

}