class PlayerManagement {

  constructor (defaultPlayers) {
    this.defaultPlayers = defaultPlayers
    this.gamepadController = new GamepadController()
  }

  registerPlayer () {
    let ret = parseInt(this.playerCounter.value)
    this.playerCounter.value = ret + 1
    return ret
  }

  addKeyInputFunctionality(el, onselect) {
    const pressKeyPlaceholder = '(press key)'
    el.addEventListener('focus', () => {
      if (el.value !== '') {
        el.dataset.oldinput = el.value
      }
      el.value = pressKeyPlaceholder
    })
    el.addEventListener('blur', () => {
      if (el.value === pressKeyPlaceholder) {
        el.value = el.dataset.oldinput
      }
    })
    el.addEventListener('keydown', (e) => {
      e.stopPropagation()
      e.preventDefault()
      el.value = e.code
      el.blur()
      if (onselect) {
        onselect()
      }
    })
    this.gamepadController.addEventListener('button-pressed', async (e) => {
      if (el === document.activeElement) {
        el.value = "gamepad-" + e.gamepad.id.replaceAll('-', '') + "-btn-" + e.button
        el.blur()
        if (onselect) {
          onselect()
        }
      }
    })
    this.gamepadController.addEventListener('axis-pressed', async (e) => {
      if (el === document.activeElement) {
        el.value = "gamepad-" + e.gamepad.id.replaceAll('-', '') + "-axis-" + e.axis + "-" + e.direction
        el.blur()
        if (onselect) {
          onselect()
        }
      }
    })
  }

  createKeyInput (onselect) {
    let el = document.createElement('input')
    this.addKeyInputFunctionality(el, onselect)
    return el
  }

  addPlayer () {
    let id = this.registerPlayer()
    console.log('Adding player:', id)

    let defaultPlayer = this.defaultPlayers[id] || { name: 'Player ' + id, color: getRandomColor() }

    let container = document.createElement('tr')
    container.className = 'player'
    container.id = 'player-' + id

    let inputName = document.createElement('input')
    inputName.type = 'text'
    inputName.className = 'player-name'
    inputName.value = defaultPlayer.name || ''
    inputName.name = 'player[' + id + '][name]'
    container.append(wrap('td', inputName))

    let inputColor = document.createElement('input')
    inputColor.type = 'color'
    inputColor.className = 'player-color'
    inputColor.value = defaultPlayer.color || ''
    inputColor.name = 'player[' + id + '][color]'
    container.append(wrap('td', inputColor))

    let inputRightKey = this.createKeyInput(() => {
      let next = container.nextElementSibling
      if (next) {
        let nextLeftInput = next.querySelector('input[data-left]')
        if (nextLeftInput && nextLeftInput.value === '') {
          nextLeftInput.focus()
        }
      }
    })
    inputRightKey.name = 'player[' + id + '][right]'
    inputRightKey.className = 'player-key-right'
    inputRightKey.placeholder = '(Click here and choose)'

    let inputLeftKey = this.createKeyInput(() => inputRightKey.focus())
    inputLeftKey.name = 'player[' + id + '][left]'
    inputLeftKey.className = 'player-key-left'
    inputLeftKey.placeholder = '(Click here and choose)'
    inputLeftKey.dataset.left = 'true'
    container.append(wrap('td', inputLeftKey))

    container.append(wrap('td', inputRightKey))

    let killPlayer = document.createElement('button')
    killPlayer.innerText = 'Remove'
    killPlayer.type = 'button'
    killPlayer.onclick = () => {
      document.getElementById('player-' + id).remove()
    }
    container.append(wrap('td', killPlayer))

    this.list.append(container)
    return { inputName: inputName, inputColor: inputColor, inputLeftKey: inputLeftKey, inputRightKey: inputRightKey }
  }

  init () {
    document.getElementById('addPlayerBtn').onclick = this.addPlayer.bind(this)
    this.list = document.getElementById('player-list')
    this.playerCounter = document.getElementById('playerCount')
    this.resetPlayerConfig()

    document.querySelectorAll("input[type='key-input']").forEach(e => this.addKeyInputFunctionality(e))
    this.gamepadController.init()
  }

  resetPlayerConfig () {
    this.list.innerHTML = ''
    this.playerCounter.value = 0
  }

  saveCurrentConfig (name) {
    this.storePlayerConfigToSessionStorage(name, this.getConfigFromDOM())
  }

  loadAndApplyConfig (name) {
    let config = this.loadPlayerConfigFromSessionStorage(name)
    if (config) {
      this.applyConfiguration(config)
    }
    return config
  }

  getConfigFromDOM () {
    let store = []
    for (let i = 0; i < this.list.children.length; i++) {
      let c = this.list.children[i]
      let name = c.querySelector('input.player-name').value
      let color = c.querySelector('input.player-color').value
      let leftKey = c.querySelector('input.player-key-left').value
      let rightKey = c.querySelector('input.player-key-right').value

      console.log(name, color, leftKey, rightKey)
      store.push({
        name: name,
        color: color,
        leftKey: leftKey,
        rightKey: rightKey
      })
    }
    return store
  }

  storePlayerConfigToSessionStorage (name, config) {
    window.sessionStorage['playerconfig-' + name] = JSON.stringify(config)
  }

  loadPlayerConfigFromSessionStorage (name) {
    let o = window.sessionStorage['playerconfig-' + name]
    if (o) {
      return JSON.parse(o)
    }
    return null
  }

  applyConfiguration (config) {
    this.resetPlayerConfig()

    for (let i = 0; i < config.length; ++i) {
      let p = config[i]
      let { inputName, inputColor, inputLeftKey, inputRightKey } = this.addPlayer()
      inputName.value = p.name
      inputColor.value = p.color
      inputLeftKey.value = p.leftKey
      inputRightKey.value = p.rightKey
    }
  }

}

function wrap (type, obj) {
  let el = document.createElement(type)
  el.append(obj)
  return el
}

function check () {
  if (document.getElementsByClassName('player').length < 2) {
    window.alert('You need at least two players to play. But get more for more fun!')
    return false
  }
  return true
}

class ItemManagement {

  prefix = 'items-'

  constructor (itemClasses, defaultUsedItems) {
    this.itemClasses = itemClasses
    this.defaultUsedItems = defaultUsedItems
  }

  showItems () {
    this.itemClasses.forEach((item) => {
      let itemNode = document.createElement('div')
      itemNode.className = 'item'
      let checkBox = document.createElement('input')
      checkBox.type = 'checkbox'
      checkBox.name = 'item-' + item.name
      itemNode.append(wrap('div', checkBox))

      itemNode.append(wrap('div', item.image))
      let descriptionNode = document.createElement('p')
      descriptionNode.innerText = item.description
      itemNode.append(descriptionNode)

      document.getElementById('item-configurator').append(itemNode)
    })
  }

  getSavedItemSets () {
    return getSavedLocalStorageList(this.prefix + 'own-')
  }

  saveItemSelectionToLocalStorage (name) {
    let itemSelection = {}
    document.querySelectorAll('#item-configurator input[type=checkbox]').forEach((el) => {
      itemSelection[el.name.substr('item-'.length)] = el.checked
    })
    window.localStorage[name] = JSON.stringify(itemSelection)
  }

  loadItemSelectionFromLocalStorage (name) {
    if (typeof window.localStorage[name] !== 'undefined') {
      return JSON.parse(window.localStorage[name])
    }
    return null
  }

  applyItemSelection (itemSelection) {
    console.log('Applying item selection: ', itemSelection)
    for (let key in itemSelection) {
      let els = document.getElementsByName('item-' + key)
      if (els.length > 0) {
        els[0].checked = itemSelection[key]
      }
    }
  }

  init () {
    console.log('Creating items')
    this.showItems()
    document.getElementById('items-selectall-btn').onclick = () => {
      document.querySelectorAll('#item-configurator input[type=checkbox]').forEach(o => o.checked = true)
    }
    document.getElementById('items-selectnone-btn').onclick = () => {
      document.querySelectorAll('#item-configurator input[type=checkbox]').forEach(o => o.checked = false)
    }
    console.log('Finished creating items')
    this.initItemSets()
  }

  initItemSets () {
    let last = this.loadItemSelectionFromLocalStorage(this.prefix + 'last')
    if (last !== null) {
      let predefinedContainer = document.getElementById('items-set-selector-predefineds')
      predefinedContainer.innerHTML += '<option value="last" selected="selected">Last Used Items</option>'
      this.applyItemSelection(last)
    } else {
      document.getElementById('items-set-selector-predefineds').children[0].selected = true
      this.applyItemSelection(this.defaultUsedItems)
    }

    document.getElementById('items-set-selector-load-btn').onclick = () => {
      let setName = document.getElementById('items-set-selector').value
      if (setName === 'default') {
        this.applyItemSelection(this.defaultUsedItems)
      } else {
        if (setName !== 'last') {
          setName = 'own-' + setName
        }

        let itemSelection = this.loadItemSelectionFromLocalStorage(this.prefix + setName)
        this.applyItemSelection(itemSelection)
      }
    }

    document.getElementById('items-set-custom-save-btn').onclick = () => {
      let name = document.getElementById('items-set-custom-name').value
      let internalName = this.prefix + 'own-' + name
      let isStored = window.localStorage[internalName]
      if (isStored) {
        if (!window.confirm('There exists already a itemset with this name. Do you want to overwrite it?')) {
          return
        }
      }

      this.saveItemSelectionToLocalStorage(internalName)

      if (!isStored) {
        let o = document.createElement('option')
        o.innerText = name
        document.getElementById('items-set-selector-individuals').append(o)
      }

      window.alert('Successfully stored item set')
    }

    // load all item sets
    let savedItemSets = this.getSavedItemSets()
    savedItemSets.forEach((i) => {
      let o = document.createElement('option')
      o.innerText = i
      document.getElementById('items-set-selector-individuals').append(o)
    })
    // TODO let player remove item sets
  }
}

class ConfigurationManagement {

  prefix = 'config-'

  constructor (defaultConfig) {
    this.defaultConfig = defaultConfig
  }

  init () {
    let config = loadConfigFromLocalStorage(this.prefix + 'last') || this.defaultConfig
    console.log('Using config:', config)
    this.applyConfig(config)

    this.initConfigSets()
  }

  applyConfig (config) {
    let _fill = undefined
    _fill = (obj, path) => {
      for (let i in obj) {
        let o = obj[i]
        if (typeof o === 'object') {
          _fill(o, path + '.' + i)
        } else {
          let els = document.getElementsByName(path + '.' + i)
          if (els.length > 0) {
            if (els[0].type === 'checkbox') {
              els[0].checked = o
            } else {
              els[0].value = o
            }
          }
        }
      }
    }
    _fill(config, 'config')
  }

  readConfig () {
    let config = this.defaultConfig
    document.querySelectorAll('.settings input').forEach(el => {
      let confPath = el.name.substr('config.'.length).split('.')
      let lastPath = confPath.pop()
      let confObj = config
      confPath.forEach((path) => {
        let o = confObj[path]
        if (!o) {
          o = {}
        }
        confObj[path] = o
        confObj = o
      })
      if (el.type === 'checkbox') {
        confObj[lastPath] = el.checked
      } else {
        confObj[lastPath] = el.value
      }
    })
    return config
  }

  saveCurrentConfig (name) {
    let config = this.readConfig()
    saveConfigToLocalStorage(config, this.prefix + name)
  }

  initConfigSets () {
    let last = loadConfigFromLocalStorage(this.prefix + 'last')
    if (last !== null) {
      let predefinedContainer = document.getElementById('config-set-selector-predefineds')
      predefinedContainer.innerHTML += '<option value="last" selected="selected">Last Used Configuration</option>'
      this.applyConfig(last)
    } else {
      document.getElementById('config-set-selector-predefineds').children[0].selected = true
      this.applyConfig(this.defaultConfig)
    }

    document.getElementById('config-set-selector-load-btn').onclick = () => {
      let setName = document.getElementById('config-set-selector').value
      if (setName === 'default') {
        this.applyConfig(this.defaultConfig)
      } else {
        if (setName !== 'last') {
          setName = 'own-' + setName
        }

        let config = loadConfigFromLocalStorage(this.prefix + setName)
        this.applyConfig(config)
      }
    }

    document.getElementById('config-set-custom-save-btn').onclick = () => {
      let name = document.getElementById('config-set-custom-name').value
      let internalName = this.prefix + 'own-' + name
      let isStored = window.localStorage[internalName]
      if (isStored) {
        if (!window.confirm('There exists already a config set with this name. Do you want to overwrite it?')) {
          return
        }
      }

      let config = this.readConfig()
      saveConfigToLocalStorage(config, internalName)

      if (!isStored) {
        let o = document.createElement('option')
        o.innerText = name
        document.getElementById('config-set-selector-individuals').append(o)
      }

      window.alert('Successfully stored config set')
    }

    // load all item sets
    let savedConfigSets = this.getSavedConfigSets()
    savedConfigSets.forEach((i) => {
      let o = document.createElement('option')
      o.innerText = i
      document.getElementById('config-set-selector-individuals').append(o)
    })
    // TODO let player remove item sets
  }

  getSavedConfigSets () {
    return getSavedLocalStorageList(this.prefix + 'own-')
  }

}

class GoalsManagement {

  selector
  configContainer

  constructor (goals, defaultGoal) {
    this.goals = goals
    this.defaultGoal = defaultGoal
  }

  init () {
    this.selector = document.getElementById('goal-selector')
    this.configContainer = document.getElementById('goal-config-container')

    for (let name in this.goals) {
      let g = this.goals[name]
      let opt = wrap('option', g.name)
      opt.value = name
      this.selector.append(opt)
    }
    this.selector.onchange = () => {
      this.loadConfigStructure(this.goals[this.selector.value].configSpec)
    }

    let lastConfig = this.readGoalConfigFromLocalStorage('last')
    if (lastConfig === null) {
      this.loadConfigStructure(this.defaultGoal.configSpec)
    } else {
      this.loadConfigStructure(lastConfig.configSpec)
      this.applyGoalConfig(lastConfig)
    }

  }

  loadConfigStructure (configSpec) {
    this.configContainer.innerHTML = ''

    console.log('Goal-ConfigSpec:', configSpec)

    for (let name in configSpec) {
      let spec = configSpec[name]
      let configContainer = document.createElement('div')
      configContainer.className = 'goal-config'

      let title = document.createElement('span')
      title.className = 'goal-title'
      title.innerText = spec.title
      configContainer.append(title)

      let input = document.createElement('input')
      input.type = spec.type
      if (typeof spec.min !== 'undefined') input.min = spec.min
      if (typeof spec.max !== 'undefined') input.max = spec.max
      if (typeof spec.step !== 'undefined') input.step = spec.step
      input.name = 'goal.config.' + name
      input.value = spec.default
      configContainer.append(input)

      let desc = document.createElement('span')
      desc.className = 'goal-description advanced'
      desc.innerText = spec.description
      configContainer.append(desc)

      this.configContainer.append(configContainer)
    }
  }

  readGoalFromDOM () {
    let goalName = this.selector.value
    let goal = { ...this.goals[goalName], config: {}, _id: goalName }
    for (let i in goal.configSpec) {
      goal.config[i] = document.getElementsByName('goal.config.' + i)[0].value
    }
    return goal
  }

  applyGoalConfig (goal) {
    this.selector.value = goal._id
    for (let i in goal.config) {
      document.getElementsByName('goal.config.' + i)[0].value = goal.config[i]
    }
  }

  storeGoalConfigInLocalStorage (goal, name) {
    window.sessionStorage['goal-' + name] = JSON.stringify(goal)
  }

  readGoalConfigFromLocalStorage (name) {
    let s = window.sessionStorage['goal-' + name]
    if (s) {
      return JSON.parse(s)
    }
    return null
  }

}

class WorldManagement {

  constructor (worlds, defaultWorld) {
    this.worlds = worlds
    this.defaultWorld = defaultWorld
  }

  init () {
    this.selector = document.getElementById('world-selector')
    this.configContainer = document.getElementById('world-config-container')

    for (let name in this.worlds) {
      let g = this.worlds[name]
      let opt = wrap('option', g.name)
      opt.value = name
      this.selector.append(opt)
    }
    this.selector.onchange = () => {
      this.loadConfigStructure(this.worlds[this.selector.value].configSpec)
    }

    let lastConfig = this.readWorldConfigFromLocalStorage('last')
    if (lastConfig === null) {
      this.loadConfigStructure(this.defaultWorld.configSpec)
    } else {
      this.loadConfigStructure(lastConfig.configSpec)
      this.applyWorldConfig(lastConfig)
    }

  }

  loadConfigStructure (configSpec) {
    this.configContainer.innerHTML = ''

    console.log('World-ConfigSpec:', configSpec)

    for (let name in configSpec) {
      let spec = configSpec[name]
      let configContainer = document.createElement('div')
      configContainer.className = 'world-config'

      let title = document.createElement('span')
      title.className = 'world-title'
      configContainer.append(title)

      let input = document.createElement('input')
      input.type = spec.type
      if (typeof spec.min !== 'undefined') input.min = spec.min
      if (typeof spec.max !== 'undefined') input.max = spec.max
      if (typeof spec.step !== 'undefined') input.step = spec.step
      input.name = 'world.config.' + name
      input.value = spec.default
      configContainer.append(input)

      let desc = document.createElement('span')
      desc.className = 'world-description advanced'
      desc.innerText = spec.description
      configContainer.append(desc)

      this.configContainer.append(configContainer)
    }
  }

  readWorldFromDOM () {
    let worldName = this.selector.value
    let world = { ...this.worlds[worldName], config: {}, _id: worldName }
    for (let i in world.configSpec) {
      world.config[i] = document.getElementsByName('world.config.' + i)[0].value
    }
    return world
  }

  applyWorldConfig (world) {
    this.selector.value = world._id
    for (let i in world.config) {
      document.getElementsByName('world.config.' + i)[0].value = world.config[i]
    }
  }

  storeWorldConfigInLocalStorage (world, name) {
    window.sessionStorage['world-' + name] = JSON.stringify(world)
  }

  readWorldConfigFromLocalStorage (name) {
    let s = window.sessionStorage['world-' + name]
    if (s) {
      return JSON.parse(s)
    }
    return null
  }

}

function getSavedLocalStorageList (prefix) {
  let set = []
  for (let i in window.localStorage) {
    if (i.startsWith(prefix)) {
      set.push(i.substr(prefix.length))
    }
  }
  return set
}

function initAdvancedSelector() {
  document.getElementById('advanced-selector').addEventListener('change', (e) => {
    let isChecked = e.target.checked
    document.querySelectorAll('.advanced').forEach(v => {
      if (isChecked) {
        v.classList.remove('hidden')
      } else {
        v.classList.add('hidden')
      }
    })
  })
  document.getElementById('advanced-selector').checked = false
  document.querySelectorAll('.advanced').forEach(v => {
    v.classList.add('hidden')
  })
}

class GamepadController {
  constructor () {
    this.stateMap = new Map()
    this.axisPressedEventListener = []
    this.axisReleasedEventListener = []
    this.buttonPressedEventListener = []
    this.buttonReleasedEventListener = []
  }

  init() {
    setInterval(() => this.updateEvents(), 1000 / 60)
  }

  async updateEvents() {
    let gamepads = navigator.getGamepads()
    for (let i = 0; i < gamepads.length; i++) {
      let gamepad = gamepads[i]
      if (gamepad) {
        const lastState = this.stateMap.get(gamepad.id)
        const currentState = {
          buttons: gamepad.buttons.map(b => ({pressed: b.pressed})),
          axes: gamepad.axes.map(a => a.toFixed(2))
        }
        const serializedState = JSON.stringify(currentState)
        if (lastState !== serializedState) {
          this.stateMap.set(gamepad.id, serializedState)
          if (lastState) {
            const lastStateDeserialized = JSON.parse(lastState)

            for (let i = 0; i < currentState.buttons.length; i++) {
              if (currentState.buttons[i].pressed && !lastStateDeserialized.buttons[i].pressed) {
                await this.triggerEvent('button', {gamepad: gamepad, button: i, pressed: true})
              }
              if (!currentState.buttons[i].pressed && lastStateDeserialized.buttons[i].pressed) {
                await this.triggerEvent('button', {gamepad: gamepad, button: i, pressed: false})
              }
            }

            for (let i = 0; i < currentState.axes.length; i++) {
              const valuePrev = parseFloat(lastStateDeserialized.axes[i])
              const valueNow = parseFloat(currentState.axes[i])
              if (valueNow > 0.6 && valuePrev < 0.6) {
                await this.triggerEvent('axis', {gamepad: gamepad, axis: i, direction: 'positive', pressed: true})
              } else if (valueNow < 0.6 && valuePrev > 0.6) {
                await this.triggerEvent('axis', {gamepad: gamepad, axis: i, direction: 'positive', pressed: false})
              }

              if (valueNow > -0.6 && valuePrev < -0.6) {
                await this.triggerEvent('axis', {gamepad: gamepad, axis: i, direction: 'negative', pressed: true})
              } else if (valueNow < -0.6 && valuePrev > -0.6) {
                await this.triggerEvent('axis', {gamepad: gamepad, axis: i, direction: 'negative', pressed: false})
              }
            }
          }
        }
      }
    }
  }

  async triggerEvent (type, event) {
    if (type === 'button' && event.pressed) {
      for (const l of this.buttonPressedEventListener) {
        await l(event)
      }
    } else if (type === 'button' && !event.pressed) {
      for (const l of this.buttonReleasedEventListener) {
        await l(event)
      }
    } else if (type === 'axis' && event.pressed) {
      for (const l of this.axisPressedEventListener) {
        await l(event)
      }
    } else if (type === 'axis' && !event.pressed) {
      for (const l of this.axisReleasedEventListener) {
        await l(event)
      }
    }
  }

  addEventListener (spec, listener) {
    const [type, pressed] = spec.split("-")
    if (type === 'button') {
      if (pressed === 'pressed') {
        this.buttonPressedEventListener.push(listener)
      } else if (pressed === "released") {
        this.buttonReleasedEventListener.push(listener)
      }
    } else if (type === 'axis') {
      if (pressed === "pressed") {
        this.axisPressedEventListener.push(listener)
      } else if (pressed === "released") {
        this.axisReleasedEventListener.push(listener)
      }
    }
  }
}