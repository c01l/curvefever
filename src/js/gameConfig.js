const defaultConfig = {
  color: {
    background: '#000000',
    world: '#ffff00',
    playerHead: '#ffff99',
    playerHeadDead: '#ff4242',
    playerHeadSwitched: '#0000ff'
  },
  world: {
    wallSpacing: 0.1
  },
  player: {
    turnSpeed: Math.PI / 100,
    holeSize: 17,
    holeDelayFixed: 200,
    holeDelayRandomMax: 100,
    initialSize: 3,
    initialSpeed: 1.4
  },
  item: {
    size: 50,
    spawnDelayFixed: 300,
    spawnDelayRandomMax: 300
  },
  control: {
    quitGameKey: 'Escape',
    pauseKey: 'Space'
  },
  effect: {
    display: {
      showInSidecar: false
    },
    player: {
      ghost: {
        canWalkThroughWorld: false,
        duration: 400
      },
      turn90: {
        duration: 400
      },
      sizeUp: {
        duration: 400,
        multiplier: 2
      },
      sizeDown: {
        duration: 400,
        multiplier: 2
      },
      speedUp: {
        duration: 120,
        multiplier: 2
      },
      slowDown: {
        duration: 400,
        multiplier: 2
      },
      partyMode: {
        duration: 400
      },
      phaseWorld: {
        duration: 800
      },
      switchKeys: {
        duration: 400
      },
      fullForce: {
        duration: 75
      },
      shielded: {
        duration: 400
      },
      ice: {
        duration: 200,
        driftFactor: 15
      },
      moonfish: {
        duration: 400,
        moonfishFactor: 15
      }
    },
    game: {
      phaseWorld: {
        duration: 400
      },
      cloud: {
        duration: 400
      },
      portal: {
        size: 75,
        duration: 1000
      }
    }
  },
  ticks: 40
}

function saveConfigToLocalStorage (config, name) {
  window.localStorage[name] = JSON.stringify(config)
}

function loadConfigFromLocalStorage (name) {
  let obj = window.localStorage[name]
  if (obj) {
    return JSON.parse(obj)
  }
  return null
}

function loadConfigFromString (str) {
  const urlParams = new URLSearchParams(str)
  const config = clone(defaultConfig)

  urlParams.forEach((value, key) => {
    const path = key.split('.')
    if (path.length > 1 && path[0] === 'config') {
      let o = config
      for (let i = 1; i < path.length - 1; ++i) {
        o = o[path[i]]
        if (typeof o === 'undefined') {
          console.error('Key does not exist in config', key)
          return
        }
      }
      const lastKey = path[path.length - 1]
      const oldVarType = typeof o[lastKey]

      switch (oldVarType) {
        case 'undefined':
          console.error('Key does not exist in config', key)
          return
        case 'number':
          o[lastKey] = Number(value)
          break
        case 'boolean':
          o[lastKey] = value === 'true'
          break
        case 'string':
          if (o[lastKey][0] === '#') {
            value = '#' + trim(value, '#')
          }
          o[lastKey] = value
          break
        default:
          console.error('Unknown variable type', oldVarType, key)
      }
    }
  })

  return config
}