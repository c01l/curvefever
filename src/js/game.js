const TURNMODE_NORMAL = 0
const TURNMODE_90DEG = 1

const GAMESTATE_START = 0
const GAMESTATE_PAUSED = 1
const GAMESTATE_ROUNDOVER = 2
const GAMESTATE_RUNNING = 3
const GAMESTATE_GAMEOVER = 4

class Game {
  movementCanvas
  worldCollisionMapCanvas
  gMove
  gWorld

  players = []
  items = []
  itemSpawnCounter = 0
  usedItems = []
  effects = []
  collision = {
    world: true
  }
  gameState = GAMESTATE_START
  goal = {}
  roundCounter = 1
  portals = []
  cloudMode = false

  constructor (config, usedItems, goal, world, keyManager, uiManager) {
    this.config = config
    this.usedItems = usedItems
    this.goal = goal

    this.movementCanvas = document.createElement('canvas')
    this.worldCollisionMapCanvas = document.createElement('canvas')

    this.world = world
    this.keyManager = keyManager
    this.uiManager = uiManager
  }

  getGoal () {
    return this.goal
  }

  getConfig () {
    return this.config
  }

  setSize (width, height) {
    this.movementCanvas.width = width
    this.movementCanvas.height = height
    this.worldCollisionMapCanvas.width = width
    this.worldCollisionMapCanvas.height = height

    this.world.setup.call(this.world, width, height)

    console.log('Gamesize: ', width, height)
  }

  init () {
    this.gMove = this.movementCanvas.getContext('2d')
    this.gWorld = this.worldCollisionMapCanvas.getContext('2d')
  }

  setPlayers (players) {
    this.players = players
    this.resetPlayerArray()
    this.resetGame()
  }

  addPlayer(player) {
    this.players.push(this.initializePlayer(player))
  }

  togglePause () {
    if (this.gameState === GAMESTATE_RUNNING) {
      this.updateGameState(GAMESTATE_PAUSED)
    } else if (this.gameState === GAMESTATE_PAUSED) {
      this.updateGameState(GAMESTATE_RUNNING)
    }
  }

  gameLoop () {
    if (this.gameState === GAMESTATE_RUNNING) {
      this.adaptPlayerDirections()
      this.adaptPlayerHoles()
      this.spawnItems()
      this.updateItemEffects()
      this.checkPlayerItemInteraction()
      this.movePlayers() // needs to be before check player death in case item spawns a bot
      this.checkPlayerDeaths()
      this.drawMovementPaths()
      // this.updatePortals()
      this.checkGameOver()
    } else if (this.gameState === GAMESTATE_ROUNDOVER) {
      let winner = this.goal.getWinner.call(this.goal, this)
      if (winner !== null) {
        this.updateGameState(GAMESTATE_GAMEOVER)
        this.uiManager.setMessage(winner.name + ' won the game!')
      }
    }
  }

  updateGameState (newState) {
    this.gameState = newState
    switch (this.gameState) {
      case GAMESTATE_GAMEOVER:
        this.uiManager.setGameState('Game Over')
        break
      case GAMESTATE_START:
        this.uiManager.setGameState('Starting')
        break
      case GAMESTATE_RUNNING:
        this.uiManager.setGameState('Running')
        break
      case GAMESTATE_ROUNDOVER:
        this.uiManager.setGameState('Round Over')
        break
      case GAMESTATE_PAUSED:
        this.uiManager.setGameState('Paused')
        break
    }
  }

  resetGame () {
    this.updateGameState(GAMESTATE_START)

    this.initWorld(this.world)
    this.gMove.clear()
    this.items = []
    this.stopAllEffects()
    this.itemSpawnCounter = this.config.item.spawnDelayFixed + Math.random() * this.config.item.spawnDelayRandomMax
    this.resetPlayerArray()
  }

  resetPlayerArray () {
    this.players = this.players.filter(p => !p.bot).map(p => this.initializePlayer(p))
  }

  getNewHoleCounter () {
    return this.config.player.holeDelayFixed + Math.random() * this.config.player.holeDelayRandomMax
  }

  getRandomGameAreaPosition () {
    return {
      x: Math.random() * this.worldCollisionMapCanvas.width,
      y: Math.random() * this.worldCollisionMapCanvas.height
    }
  }

  getRandomStartStateInGameArea () {
    let state
    do {
      state = this.getRandomGameAreaPosition()
    } while (!this.world.isPlayerSpawnable.call(this.world, state.x, state.y, state.direction))
    state['direction'] = randomAngle()
    return state
  }

  getRandomItemPositionInWorld () {
    let condition = this.world.isItemSpawnable || this.world.isPlayerSpawnable
    let state
    do {
      state = this.getRandomGameAreaPosition()
    } while (!condition.call(this.world, state.x, state.y, 0))
    return state
  }

  initializePlayer (basic) {
    let startState = this.getRandomStartStateInGameArea()
    return {
      ...basic,
      size: this.config.player.initialSize,
      drawColor: basic.color,
      pos: {
        x: startState.x,
        y: startState.y,
        ox: undefined,
        oy: undefined,
        ocx: null,
        ocy: null
      },
      direction: startState.direction,
      speed: this.config.player.initialSpeed,
      isDead: false,
      holeCounter: this.getNewHoleCounter(),
      turnMode: TURNMODE_NORMAL,
      collision: {
        world: true,
        paths: true
      },
      invisible: false,
      effects: [],
      animation: {},
      controls: {
        ...basic.controls,
        switched: false,
        disabled: false
      },
      shielded: false,
      driftFactor: 0,
      moonfishFactor: 0
    }
  }

  drawMovementPaths () {
    this.players.forEach(p => {
      if (!p.invisible && p.pos.ox && p.pos.oy) {
        this.gMove.beginPath()
        this.gMove.moveTo(p.pos.ox, p.pos.oy)
        this.gMove.lineTo(p.pos.x, p.pos.y)
        this.gMove.strokeStyle = p.drawColor
        this.gMove.lineWidth = p.size * 2
        this.gMove.stroke()
      }
      p.pos.ox = p.pos.x
      p.pos.oy = p.pos.y
    })
  }

  spawnItems () {
    this.itemSpawnCounter--
    if (this.itemSpawnCounter < 0) {
      this.itemSpawnCounter = this.config.item.spawnDelayFixed + Math.random() * this.config.item.spawnDelayRandomMax

      if (this.usedItems.length > 0) {
        this.spawnRandomItem()
      }
    }
  }

  spawnRandomItem () {
    let i = this.usedItems[Math.floor(Math.random() * this.usedItems.length)]
    let itemPos = this.getRandomItemPositionInWorld()
    this.items.push(this.instantiateNewItem(i, {
      pos: { x: itemPos.x, y: itemPos.y }
    }))
  }

  checkPlayerItemInteraction () {
    this.players.filter(p => !p.isDead).forEach((p) => {
      this.items.forEach((item, index, object) => {
        if (this.checkPlayerItemCollision(p, item)) {
          item.onHit(p, this)
          object.splice(index, 1)
        }
      })
    })
  }

  updateItemEffects () {
    this.effects.forEach((effect, index, object) => {
      if (effect.durationCounter === 0) {
        let tamperList = object.filter(e => e !== effect).map(e => e.tampers).flat()
        effect.onStart.call(effect, this, (param) => tamperList.includes(param))
      }
      effect.durationCounter++
      effect.onTick.call(effect, this)
      if (effect.durationCounter >= effect.duration) {
        // effect expired
        object.splice(index, 1)
        let tamperList = object.map(e => e.tampers).flat()
        effect.onEnd.call(effect, this, (param) => tamperList.includes(param))
      }
    })

    this.players.forEach((p) => {
      p.effects.forEach((effect, index, object) => {
        if (effect.durationCounter === 0) {
          let tamperList = object.filter(e => e !== effect).map(e => e.tampers).flat()
          effect.onStart.call(effect, p, this, (param) => tamperList.includes(param))
        }
        effect.durationCounter++
        effect.onTick.call(effect, p, this)
        if (effect.durationCounter >= effect.duration) {
          // effect expired
          object.splice(index, 1)
          let tamperList = object.map(e => e.tampers).flat()
          effect.onEnd.call(effect, p, this, (param) => tamperList.includes(param))
        }
      })
    })
  }

  stopAllEffects () {
    this.effects.forEach((e, index, object) => {
      object.splice(index, 1)
      let tamperList = object.map(e => e.tampers).flat()
      e.onEnd.call(e, this, (param) => tamperList.includes(param))
    })

    this.players.forEach(p => {
      p.effects.forEach((e, index, object) => {
        object.splice(index, 1)
        let tamperList = object.map(e => e.tampers).flat()
        e.onEnd.call(e, p, this, (param) => tamperList.includes(param))
      })
    })
  }

  checkPlayerItemCollision (p, i) {
    let dx = p.pos.x - i.pos.x
    let dy = p.pos.y - i.pos.y
    let distance = this.config.item.size / 2 + p.size
    return dx * dx + dy * dy < distance * distance
  }

  instantiateNewItem (item, options) {
    return { ...item, ...options }
  }

  checkGameOver () {
    let remainingPlayers = this.players.filter(p => !p.isDead && !p.bot)
    if (remainingPlayers.length === 1) {
      this.updateGameState(GAMESTATE_ROUNDOVER)
      this.roundCounter++

      remainingPlayers[0].points += this.players.filter(p => !p.bot).length - 1
      this.uiManager.renderPlayerPoints(this.players) // TODO move
    }
  }

  calculateAndAddPoints (player) {
    player.points += this.players.filter(p => p.isDead && !p.bot).length - 1
  }

  adaptPlayerHoles () {
    this.players.forEach(p => {
      if (p.holeCounter < 0) {
        p.drawColor = '#00000000'
        if (p.holeCounter < -this.config.player.holeSize) {
          p.holeCounter = this.getNewHoleCounter()
          p.drawColor = p.color
        }
      }
      p.holeCounter--
    })
  }

  checkPlayerDeaths () {
    const DEATH_CHECKING_ACCURECY = 3
    let newDeath = false
    this.players
      .forEach(p => {
        if (p.isDead) {
          return // because of manipulations
        }

        if (p.collision.paths) {
          let collPlayer = this.players
            .filter(po => po !== p)
            .filter(po => !po.isDead)
            .find((po) => {
              let dx = po.pos.x - p.pos.x
              let dy = po.pos.y - p.pos.y

              if (dx * dx + dy * dy < (p.size + po.size) * (p.size + po.size)) {
                // determine who should die

                let lx = po.pos.x - p.pos.x
                let ly = po.pos.y - p.pos.y

                let nx = Math.sin(po.direction)
                let ny = -Math.cos(po.direction)

                let r = lx * po.pos.y - po.pos.x * ly
                let s = po.pos.x * ny - po.pos.y * nx
                let d = r / s

                let zx = d * nx + p.pos.x
                let zy = d * ny + p.pos.y

                let cx = po.pos.x - zx
                let cy = po.pos.y - zy

                let podx = Math.cos(po.direction)
                let pody = Math.sin(po.direction)

                let e = cx * podx + cy * pody
                if (e > 0) {
                  p.isDead = true
                  this.calculateAndAddPoints(p)
                } else {
                  po.isDead = true
                  this.calculateAndAddPoints(po)
                }
                return po
              }
            })
          if (collPlayer) {
            newDeath = true
            return
          }
        }

        const checkAngle = p.turnMode === TURNMODE_90DEG ? 0.1 : 0.3 // smaller window for 90 deg turns, as we have self collisions then
        const startAngle = p.moveDirection - checkAngle / 2 * Math.PI * 2
        const endAngle = p.moveDirection + checkAngle / 2 * Math.PI * 2
        for (let i = 0; i < DEATH_CHECKING_ACCURECY; ++i) {
          let angle = interpolate(startAngle, endAngle, i / DEATH_CHECKING_ACCURECY)
          let dx = Math.cos(angle) * p.size
          let dy = Math.sin(angle) * p.size

          let cx = p.pos.x + dx
          let cy = p.pos.y + dy

          // Player-Player-Collision
          if (p.collision.paths) {
            let cd = this.gMove.getImageData(cx, cy, 1, 1).data
            if (cd[0] + cd[1] + cd[2] !== 0 && !this.isNextToAnyPortal({...p, pos: {x: cx, y: cy}})) {
              p.isDead = true
              break
            }
          }

          // Player-World-Collision
          if (p.collision.world && this.collision.world) {
            let cd2 = this.gWorld.getImageData(cx, cy, 1, 1).data
            if (cd2[0] + cd2[1] + cd2[2] !== 0 && !this.isNextToAnyPortal({...p, pos: {x: cx, y: cy}})) {
              p.isDead = true
              break
            }
          }
        }

        if (p.isDead) {
          this.calculateAndAddPoints(p)
          newDeath = true
        }
      })

    if (this.players.filter(p => !p.isDead && !p.bot).length === 0) {
      // if no real player is alive -> kill all bots
      this.players.filter(p => p.bot).forEach(p => {
        p.isDead = true
      })
    }

    if (newDeath) {
      this.uiManager.renderPlayerPoints(this.players)
    }
  }

  adaptPlayerDirections () {
    this.players.filter(p => !p.controls.disabled).filter(p => !p.bot).forEach(p => {
      let leftKey = p.controls.switched ? p.controls.right : p.controls.left
      let rightKey = p.controls.switched ? p.controls.left : p.controls.right

      p.oldDirection = p.direction

      if (p.turnMode === TURNMODE_NORMAL) {
        const moveLeft = this.keyManager.getState(leftKey)
        const moveRight = this.keyManager.getState(rightKey)
        if (moveLeft && !moveRight) {
          if (p.moonfishFactor > 0) {
            p.usedTurnSpeed = Math.max(-this.config.player.turnSpeed, p.usedTurnSpeed - this.config.player.turnSpeed / p.moonfishFactor)
          } else {
            p.usedTurnSpeed = -this.config.player.turnSpeed
          }
        }
        if (moveRight && !moveLeft) {
          if (p.moonfishFactor > 0) {
            p.usedTurnSpeed = Math.min(this.config.player.turnSpeed, p.usedTurnSpeed + this.config.player.turnSpeed / p.moonfishFactor)
          } else {
            p.usedTurnSpeed = this.config.player.turnSpeed
          }
        }

        if (!moveLeft && !moveRight) {
          if (p.moonfishFactor > 0) {
            p.usedTurnSpeed = p.usedTurnSpeed * (1 - 1 / p.moonfishFactor)
          } else {
            p.usedTurnSpeed = 0
          }
        }

        p.direction += p.usedTurnSpeed
      } else if (p.turnMode === TURNMODE_90DEG) {
        if (this.keyManager.isKeyHit(leftKey)) {
          p.direction -= Math.PI / 2
          this.keyManager.invalidateKeyHit(leftKey)
        }
        if (this.keyManager.isKeyHit(rightKey)) {
          p.direction += Math.PI / 2
          this.keyManager.invalidateKeyHit(rightKey)
        }
      }
    })
    this.players.filter(p => !p.controls.disabled && !p.isDead).filter(p => p.bot).forEach(p => {
      p.oldDirection = p.direction
      p.direction = p.bot(p, this)
    })
  }

  adaptPlayerPositionAccordingToWorldBorderRules (p) {
    if (p.pos.x < 0) {
      p.pos.x = this.worldCollisionMapCanvas.width - 1
      p.pos.ox = p.pos.x
    }
    if (p.pos.y < 0) {
      p.pos.y = this.worldCollisionMapCanvas.height - 1
      p.pos.oy = p.pos.y
    }
    if (p.pos.x > this.worldCollisionMapCanvas.width) {
      p.pos.x = 0
      p.pos.ox = p.pos.x
    }
    if (p.pos.y > this.worldCollisionMapCanvas.height) {
      p.pos.y = 0
      p.pos.oy = p.pos.y
    }
  }

  calcPortalEndpoints (p) {
    const portalSize = this.config.effect.game.portal.size / 2
    const dx = Math.cos(p.angle) * portalSize
    const dy = Math.sin(p.angle) * portalSize

    return {
      e1: {
        x: p.x + dx,
        y: p.y + dy
      },
      e2: {
        x: p.x - dx,
        y: p.y - dy
      }
    }
  }

  checkPortalTeleportation (p) {
    if (typeof p.pos.ox === 'undefined' || typeof p.pos.oy === 'undefined') {
      return
    }

    const checkPortalIntersection = (player, pcoord) => {
      const inter = line_intersect(player.pos.ox, player.pos.oy, player.pos.x, player.pos.y, pcoord.e1.x, pcoord.e1.y, pcoord.e2.x, pcoord.e2.y)
      return {isIntersecting: inter.seg1 && inter.seg2, ua: inter.ua, ub: inter.ub}
    }

    const movePlayerToOtherPortal = (player, from, to) => {
      const angle_diff = to.angle - from.angle
      player.direction += angle_diff

      const fromCoord = this.calcPortalEndpoints(from)
      const inter = checkPortalIntersection(player, fromCoord)

      const toCoord = this.calcPortalEndpoints(to)

      const posOnPortal = inter.ub
      const out_x = toCoord.e1.x + (toCoord.e2.x - toCoord.e1.x) * posOnPortal
      const out_y = toCoord.e1.y + (toCoord.e2.y - toCoord.e1.y) * posOnPortal


      player.pos.x = out_x + Math.cos(player.direction) * 0.1
      player.pos.y = out_y + Math.sin(player.direction) * 0.1
      player.pos.ox = undefined
      player.pos.oy = undefined
    }

    this.portals.forEach(po => {
      const p1Coord = this.calcPortalEndpoints(po.p1)
      const p2Coord = this.calcPortalEndpoints(po.p2)

      if (checkPortalIntersection(p, p1Coord).isIntersecting) {
        // teleport to p2
        movePlayerToOtherPortal(p, po.p1, po.p2)
      } else if (checkPortalIntersection(p, p2Coord).isIntersecting) {
        // teleport to p1
        movePlayerToOtherPortal(p, po.p2, po.p1)
      } else {
        // no teleportation needed
      }
    })

  }

  isNextToAnyPortal(player) {
    let ret = false
    this.portals.forEach(p => {
      if (ret) return
      if (this.isNextToPortal(player, p.p1)) {
        ret = true
      } else if (this.isNextToPortal(player, p.p2)) {
        ret = true
      }
    })
    return ret
  }

  isNextToPortal(player, pp) {
    const pcoords = this.calcPortalEndpoints(pp)
    const dist = line_segment_distance(player.pos.x, player.pos.y, pcoords.e1.x, pcoords.e1.y, pcoords.e2.x, pcoords.e2.y)
    return dist < player.size
  }

  movePlayers () {
    this.players
      .filter(p => !p.isDead)
      .forEach(p => {
        if (p.driftFactor !== 0) {
          const shift = 1 / (1 + p.driftFactor)
          p.moveDirection = (p.direction * shift + p.moveDirection * (1 - shift))
        } else {
          p.moveDirection = p.direction
        }

        let dx = Math.cos(p.moveDirection) * p.speed
        let dy = Math.sin(p.moveDirection) * p.speed

        p.pos.omx = p.pos.x
        p.pos.omy = p.pos.y
        p.pos.x += dx
        p.pos.y += dy

        this.adaptPlayerPositionAccordingToWorldBorderRules(p)
        this.checkPortalTeleportation(p)

      })
  }

  initWorld (world) {
    let w = this.worldCollisionMapCanvas.width
    let h = this.worldCollisionMapCanvas.height

    this.gWorld.fillStyle = '#00000000'
    this.gWorld.fillRect(0, 0, w, h)

    this.gWorld.fillStyle = this.config.color.world
    this.gWorld.strokeStyle = this.config.color.world
    this.gWorld.lineWidth = 5
    world.initialDraw.call(world, this.gWorld)
  }

  addPlayerEffect (p, name) {
    if (typeof playerEffectClasses[name] === 'undefined') {
      console.error('Unknown player effect:', name)
      return
    }

    let duration
    if (typeof this.config.effect.player[name] === 'undefined') {
      console.log('Player effect duration not set! Using global one!')
      duration = playerEffectClasses[name].duration
    } else {
      duration = this.config.effect.player[name].duration
    }

    p.effects.push(createEffect(playerEffectClasses[name], duration))
  }

  addGameEffect (name) {
    if (typeof gameEffectClasses[name] === 'undefined') {
      console.error('Unknown player effect:', name)
      return
    }

    let duration
    if (typeof this.config.effect.game[name] === 'undefined') {
      console.log('Game effect duration not set! Using global one!')
      duration = gameEffectClasses[name].duration
    } else {
      this.config.effect.game[name].duration
    }

    this.effects.push(createEffect(gameEffectClasses[name], duration))
  }

  nextPortalId = 0

  isInGameField(x, y) {
    return 0 < x && x < this.movementCanvas.width && 0 < y && y < this.movementCanvas.height
  }

  spawnPortal () {
    const portal = {
      id: this.nextPortalId++,
      p1: null,
      p2: null,
      duration: this.config.effect.game.portal.duration,
      durationCounter: 0
    }

    let coords
    do {
      portal.p1 = { ...this.getRandomGameAreaPosition(), angle: randomAngle() }
      coords = this.calcPortalEndpoints(portal.p1)
    } while(!this.isInGameField(coords.e1.x, coords.e1.y) || !this.isInGameField(coords.e2.x, coords.e2.y))

    do {
      portal.p2 = { ...this.getRandomGameAreaPosition(), angle: randomAngle() }
      coords = this.calcPortalEndpoints(portal.p2)
    } while(!this.isInGameField(coords.e1.x, coords.e1.y) || !this.isInGameField(coords.e2.x, coords.e2.y))

    console.log(portal)
    this.portals.push(portal)
    return portal.id
  }
}