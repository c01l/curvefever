function loadImage (url) {
  let i = new Image()
  i.src = url
  // TODO loading bar with onload
  i.onload = () => {}
  return i
}

function getRandomColor () {
  var letters = '0123456789ABCDEF'
  var color = '#'
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

function randomFloat (min, max) {
  return min + Math.random() * (max - min)
}

function randomInt (min, max) {
  return clamp(Math.floor(randomFloat(min, max)), min, max)
}

function randomAngle () {
  return randomFloat(0, Math.PI * 2)
}

function line_intersect(x1, y1, x2, y2, x3, y3, x4, y4)
{
  var ua, ub, denom = (y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1);
  if (denom === 0) {
    return null;
  }
  ua = ((x4 - x3)*(y1 - y3) - (y4 - y3)*(x1 - x3))/denom;
  ub = ((x2 - x1)*(y1 - y3) - (y2 - y1)*(x1 - x3))/denom;
  return {
    x: x1 + ua * (x2 - x1),
    y: y1 + ua * (y2 - y1),
    ua: ua,
    ub: ub,
    seg1: ua >= 0 && ua <= 1,
    seg2: ub >= 0 && ub <= 1
  };
}

// From: https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
function line_segment_distance(x, y, x1, y1, x2, y2) {

  const A = x - x1;
  const B = y - y1;
  const C = x2 - x1;
  const D = y2 - y1;

  const dot = A * C + B * D;
  const len_sq = C * C + D * D;
  let param = -1;
  if (len_sq !== 0) //in case of 0 length line
    param = dot / len_sq;

  let xx, yy;

  if (param < 0) {
    xx = x1;
    yy = y1;
  }
  else if (param > 1) {
    xx = x2;
    yy = y2;
  }
  else {
    xx = x1 + param * C;
    yy = y1 + param * D;
  }

  const dx = x - xx;
  const dy = y - yy;
  return Math.sqrt(dx * dx + dy * dy);
}

// Src: https://stackoverflow.com/questions/26156292/trim-specific-character-from-a-string
const trim = (str, chars) => str.split(chars).filter(Boolean).join(chars)

// Src: https://stackoverflow.com/questions/122102/what-is-the-most-efficient-way-to-deep-clone-an-object-in-javascript
const clone = (a) => JSON.parse(JSON.stringify(a))

CanvasRenderingContext2D.prototype.clear =
  CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
    if (preserveTransform) {
      this.save()
      this.setTransform(1, 0, 0, 1, 0, 0)
    }

    this.clearRect(0, 0, this.canvas.width, this.canvas.height)

    if (preserveTransform) {
      this.restore()
    }
  }

CanvasRenderingContext2D.prototype.drawImageRotated =
  CanvasRenderingContext2D.prototype.drawImageRotated || function (image, x, y, angle) {
    this.save()
    this.translate(x, y)
    this.rotate(angle)
    this.drawImage(image, 0, 0)
    this.restore()
  }

function interpolate (start, end, percent) {
  return start + (end - start) * percent
}

// Src: https://stackoverflow.com/questions/35969656/how-can-i-generate-the-opposite-color-according-to-current-color
function invertHex (hex) {
  return (Number(`0x1${hex}`) ^ 0xFFFFFF).toString(16).substr(1).toUpperCase()
}

function invertColorStr (colorStr) {
  let hex = colorStr.substr(1)
  return '#' + invertHex(hex)
}

function animate (animStore, name, low, high, speed) {
  let obj = animStore[name]
  if (!obj) {
    obj = animStore[name] = {}
  }
  let dir = obj.dir || 'up'
  let current = obj.value || low

  if (dir === 'up') {
    current += speed
    if (current > high) {
      obj.dir = 'down'
    }
  } else {
    current -= speed
    if (current < low) {
      obj.dir = 'up'
    }
  }

  obj.value = current
  return current
}

function clamp (x, min, max) {
  if (x < min) return min
  if (x > max) return max
  return x
}

function setColorStringAlpha (colorString, alpha) {
  let hex = colorString.substr(1)
  hex = hex.substr(-6)
  if (typeof alpha == 'number') {
    let v = Math.floor(alpha * 255)
    alpha = Number(clamp(v, 0, 255)).toString(16).toUpperCase()
  }

  return '#' + alpha + hex
}

// Src: https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
function shuffle (array) {
  let counter = array.length

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter)

    // Decrease counter by 1
    counter--

    // And swap the last element with it
    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

// Src: https://stackoverflow.com/questions/3115982/how-to-check-if-two-arrays-are-equal-with-javascript
function arraysEqual (a, b) {
  if (a === b) return true
  if (a == null || b == null) return false
  if (a.length !== b.length) return false

  // If you don't care about the order of the elements inside
  // the array, you should sort both arrays here.
  // Please note that calling sort on an array will modify that array.
  // you might want to clone your array first.

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false
  }
  return true
}

function arrayHasEqual(a, b) {
  if (a.length !== b.length) return false
  for (let i = 0; i < a.length; ++i) {
    if (a[i] === b[i]) return true;
  }
  return false;
}