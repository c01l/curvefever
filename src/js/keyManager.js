class KeyManager {
  keyState = {}
  keyHitState = {}
  keyHitBlockState = {}

  init () {
    const keyEventLogger = e => {
      e.preventDefault()
      if (e.type === 'keydown') {
        if (!this.keyHitBlockState[e.code]) {
          this.keyHitState[e.code] = true
          this.keyHitBlockState[e.code] = true
        }
      } else if (e.type === 'keyup') {
        this.keyHitBlockState[e.code] = false
      }
      this.keyState[e.code] = e.type === 'keydown'
    }
    window.addEventListener('keydown', keyEventLogger)
    window.addEventListener('keyup', keyEventLogger)
  }

  getState (key) {
    if (key.startsWith('gamepad-')) {
      const split = key.split('-')
      const id = split[1]
      const gamepad = navigator.getGamepads().filter(g => g.id.replaceAll('-', '') === id)[0]
      if (!gamepad) {
        console.log('Gamepad not found', id, navigator.getGamepads())
        return false
      }
      if (split[2] === 'btn') {
        const btnIndex = parseInt(split[3])
        return gamepad.buttons[btnIndex].pressed
      } else if (split[2] === 'axis') {
        const axisIndex = parseInt(split[3])
        if (split[4] === 'positive') {
          return gamepad.axes[axisIndex] > 0.5
        } else if (split[4] === 'negative') {
          return gamepad.axes[axisIndex] < -0.5
        } else {
          console.log("Invalid gamepad axis direction:", split[4])
        }
      } else {
        console.error('Invalid gamepad button type:', split[2])
      }
    }
    return this.keyState[key] || false
  }

  isKeyHit (key) {
    return this.keyHitState[key] || false
  }

  invalidateKeyHit (key) {
    this.keyHitState[key] = false
  }
}