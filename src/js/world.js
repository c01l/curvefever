const worlds = {
  box: {
    name: 'Box',
    configSpec: {
      wallSpacing: {
        description: 'Percentage of the game field size that players and items are not spawned at. The goal is to have an area around the border, where stuff will not spawn to prevent "dead spawning".',
        type: 'number',
        default: 0.1,
        step: 'any',
        min: 0
      }
    },
    setup: function (w, h) {
      this.w = w
      this.h = h
    },
    isPlayerSpawnable: function (x, y, direction) {
      return this.w * this.config.wallSpacing < x
        && x < this.w * (1 - this.config.wallSpacing)
        && this.h * this.config.wallSpacing < y
        && y < this.h * (1 - this.config.wallSpacing)
    },
    initialDraw: function (g) {
      g.beginPath()
      g.rect(g.lineWidth / 2, g.lineWidth / 2, this.w - g.lineWidth, this.h - g.lineWidth)
      g.stroke()
    }
  },
  circle: {
    name: 'Circle',
    configSpec: {
      wallSpacing: {
        description: 'Percentage of the game field size that players and items are not spawned at. The goal is to have an area around the border, where stuff will not spawn to prevent "dead spawning".',
        type: 'number',
        default: 0.1,
        step: 'any',
        min: 0
      }
    },
    setup: function (w, h) {
      this.radius = Math.min(w, h) / 2
    },
    isPlayerSpawnable: function (x, y, direction) {
      let mx, my
      mx = my = this.radius
      let dx = mx - x
      let dy = my - y
      let r = this.radius * (1 - this.config.wallSpacing)

      return dx * dx + dy * dy < r * r
    },
    isItemSpawnable: function (x, y) {
      // same as box world
      return this.radius * 2 * this.config.wallSpacing < x
        && x < this.radius * 2 * (1 - this.config.wallSpacing)
        && this.radius * 2 * this.config.wallSpacing < y
        && y < this.radius * 2 * (1 - this.config.wallSpacing)
    },
    initialDraw: function (g) {
      g.beginPath()
      g.arc(this.radius, this.radius, this.radius - g.lineWidth / 2, 0, 2 * Math.PI)
      g.stroke()
    }
  },
  void: {
    name: 'The Void',
    configSpec: {},
    setup: () => {},
    isPlayerSpawnable: () => true,
    initialDraw: () => {}
  },
  boxNoCenter: {
    name: 'Box (No center)',
    configSpec: {
      wallSpacing: {
        description: 'Percentage of the game field size that players and items are not spawned at. The goal is to have an area around the border, where stuff will not spawn to prevent "dead spawning".',
        type: 'number',
        default: 0.1,
        step: 'any',
        min: 0
      },
      centerSize: {
        description: 'The size of the center cutout, where nothing can spawn.',
        type: 'number',
        min: 10,
        default: 100
      },
      spawnItemsMiddle: {
        description: 'Spawn items in the middle?',
        type: 'checkbox',
        default: true
      },
      fillMiddle: {
        description: 'Fill middle?',
        type: 'checkbox',
        default: false
      }
    },
    setup: function (w, h) {
      this.w = w
      this.h = h
    },
    isPlayerSpawnable: function (x, y, direction) {
      let mx = this.w / 2
      let my = this.h / 2
      let dx = x - mx
      let dy = y - my

      return this.w * this.config.wallSpacing < x
        && x < this.w * (1 - this.config.wallSpacing)
        && this.h * this.config.wallSpacing < y
        && y < this.h * (1 - this.config.wallSpacing)
        && dx * dx + dy * dy > this.config.centerSize * this.config.centerSize
    },
    isItemSpawnable: function (x, y) {
      let mx = this.w / 2
      let my = this.h / 2
      let dx = x - mx
      let dy = y - my

      let cond = true
      if (!this.config.spawnItemsMiddle) {
        cond = dx * dx + dy * dy > this.config.centerSize * this.config.centerSize
      }

      return this.w * this.config.wallSpacing < x
        && x < this.w * (1 - this.config.wallSpacing)
        && this.h * this.config.wallSpacing < y
        && y < this.h * (1 - this.config.wallSpacing)
        && cond
    },
    initialDraw: function (g) {
      g.beginPath()
      g.rect(g.lineWidth / 2, g.lineWidth / 2, this.w - g.lineWidth, this.h - g.lineWidth)
      g.stroke()

      g.beginPath()
      g.arc(this.w / 2, this.h / 2, this.config.centerSize - g.lineWidth / 2, 0, 2 * Math.PI)
      if (this.config.fillMiddle) {
        g.fill()
      } else {
        g.stroke()
      }
    }
  },
  spiral: {
    name: 'Spiral',
    configSpec: {
      wallSpacing: {
        description: 'Percentage of the game field size that players and items are not spawned at. The goal is to have an area around the border, where stuff will not spawn to prevent "dead spawning".',
        type: 'number',
        default: 0.1,
        step: 'any',
        min: 0
      }
    },
    setup: function (w, h) {
      this.w = w
      this.h = h
      this.centerSize = 200
    },
    isPlayerSpawnable: function (x, y, direction) {
      const cx = this.w / 2
      const cy = this.h / 2
      const dx = cx - x
      const dy = cy - y
      return dx * dx + dy * dy < this.centerSize * this.centerSize
    },
    initialDraw: function (g) {

      const arms = 1
      const angle = Math.PI * arms / 15
      for (let i = 0; i < arms; ++i) {
        let a = 2 * Math.PI * i / arms
        g.beginPath()
        g.moveTo(this.w / 2, this.h / 2)
        let cx = this.w / 2
        let cy = this.h / 2
        for (let j = 0; j < this.w / 2; ++j) {
          cx += Math.cos(a) * j
          cy += Math.sin(a) * j
          const dcx = cx - (this.w / 2)
          const dcy = cy - (this.h / 2)
          if (dcx * dcx + dcy * dcy < this.centerSize * this.centerSize || Math.random() < 0.5) {
            g.moveTo(cx, cy)
          } else {
            g.lineTo(cx, cy)
          }

          a += angle
        }
        g.stroke()

      }
    },
    isItemSpawnable: (x, y) => {
      return true
    }
  },
}

const defaultWorld = worlds.box

function loadWorldFromParams () {
  let params = new URLSearchParams(window.localStorage.getItem('currentGameConfig'))
  let worldName = params.get('world')

  let world = { ...worlds[worldName], config: {} }
  for (let i in world.configSpec) {
    let val = params.get('world.config.' + i)
    if (world.configSpec[i].type === 'number') {
      val = parseFloat(val.replace(',', '.'))
    }
    world.config[i] = val
  }

  return world
}