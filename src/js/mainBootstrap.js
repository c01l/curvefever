/*
Player = {
  id: number,
  name: string,
  color: string, // hex encoded color
  size: number,
  pos: {x:number, y:number},
  direction: number,
  speed: number,
  controls: {
    left: string<KeyCode>,
    right: string<KeyCode>,
    switched: bool
  },
  isDead: bool,
  holeCounter: number,
  drawColor: string,
  points: number,
  turnMode: TURNMODE_NORMAL | TURNMODE_90DEG
  collision: {
    world: bool
    paths: bool
  },
  invisible: bool
}
*/

/*
Ideas for items:
Exchange control with one other player
World Trail: Player draws new world/wall (not deletable)
walk-through-world
all-walk-through-world
rocket: shot after 1.5 seconds
 */

function initGame () {
  const config = loadConfigFromString(window.localStorage.getItem('currentGameConfig'))
  let usedItems = loadSelectedItemClasses()
  let goal = loadGoalFromParams()
  let world = loadWorldFromParams()
  let keyManager = new KeyManager()
  let uiManager = new UIManager()
  let game = new Game(config, usedItems, goal, world, keyManager, uiManager)
  let isDebug = false

  uiManager.init(game)
  game.setSize(uiManager.canvas.width, uiManager.canvas.height)
  game.init()

  keyManager.init()

  const onceKeyEventsLogger = function (e) {
    if (e.code === config.control.pauseKey) {
      if (game.gameState === GAMESTATE_ROUNDOVER) {
        game.resetGame()
      } else if (game.gameState === GAMESTATE_START) {
        game.updateGameState(GAMESTATE_RUNNING)
        uiManager.updateGoalDescription()
      } else if (game.gameState === GAMESTATE_GAMEOVER) {
        window.location.href = 'index.html?v={{VERSION}}'
      } else {
        game.togglePause()
      }
    } else if (e.code === 'KeyD' && e.shiftKey && e.altKey) {
      isDebug = !isDebug
      uiManager.setDebug(isDebug)
      console.log('Toggled debug to', isDebug)
      e.preventDefault()}
    else if (isDebug && e.code === 'KeyI') {
      console.log('Spawning item')
      game.spawnRandomItem()
    } else if (e.code === game.config.control.quitGameKey) {
      game.updateGameState(GAMESTATE_PAUSED)
      if (window.confirm('Do you really want to close the game?')) {
        window.location.href = 'index.html?v={{VERSION}}'
      }
    }
  }
  window.addEventListener('keyup', onceKeyEventsLogger)

  window.addEventListener('blur', () => {
    if (game.gameState === GAMESTATE_RUNNING) {
      game.updateGameState(GAMESTATE_PAUSED)
    }
  })

  game.resetGame()
  game.setPlayers(readPlayerStartConfig())

  uiManager.renderPlayerPoints(game.players)

  let framerate = 1000 / config.ticks
  let last = 0
  let gameTimer = function (time) {
    let lastTickTime = time - last
    uiManager.setLastTickTime(lastTickTime)
    last = time
    game.gameLoop()
    if (lastTickTime >= framerate) {
      window.requestAnimationFrame(gameTimer)
    } else {
      setTimeout(function () { window.requestAnimationFrame(gameTimer) }, framerate - lastTickTime)
    }
  }

  window.requestAnimationFrame(() => gameTimer())
  window.requestAnimationFrame(() => uiManager.drawLoop())
}
