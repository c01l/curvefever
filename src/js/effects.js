const playerEffectClasses = {
  ghost: {
    name: 'Ghost',
    onStart: (p, g) => {
      if (g.config.effect.player.ghost.canWalkThroughWorld) {
        p.collision.world = false
      }
      p.collision.paths = false
      p.invisible = true
    },
    tampers: [
      'p.collision.world',
      'p.collision.paths',
      'p.invisible'
    ],
    duration: 400,
    onEnd: (p, g, isTamperd) => {
      if (g.config.effect.player.ghost.canWalkThroughWorld && !isTamperd('p.collision.world')) {
        p.collision.world = true
      }
      if (!isTamperd('p.collision.paths')) {
        p.collision.paths = true
      }
      if (!isTamperd('p.invisible')) {
        p.invisible = false
      }
    }
  },
  turn90: {
    name: 'Turn90',
    onStart: (p) => {
      p.turnMode = TURNMODE_90DEG
    },
    tampers: [
      'p.turnMode'
    ],
    duration: 400,
    onEnd: (p, game, isTamperd) => {
      if (!isTamperd('p.turnMode')) {
        p.turnMode = TURNMODE_NORMAL
      }
    }
  },
  sizeUp: {
    name: 'Size Up',
    onStart: (p, g) => p.size *= g.config.effect.player.sizeUp.multiplier,
    onEnd: (p, g) => p.size /= g.config.effect.player.sizeUp.multiplier,
    tampers: ['p.size'],
    duration: 400
  },
  sizeDown: {
    name: 'Size Down',
    onStart: (p, g) => p.size /= g.config.effect.player.sizeDown.multiplier,
    onEnd: (p, g) => p.size *= g.config.effect.player.sizeDown.multiplier,
    tampers: ['p.size'],
    duration: 400
  },
  speedUp: {
    name: 'Speed Up',
    onStart: (p, g) => p.speed *= g.config.effect.player.speedUp.multiplier,
    onEnd: (p, g) => p.speed /= g.config.effect.player.speedUp.multiplier,
    tampers: ['p.speed'],
    duration: 200
  },
  slowDown: {
    name: 'Slow Down',
    onStart: (p, g) => p.speed /= g.config.effect.player.slowDown.multiplier,
    onEnd: (p, g) => p.speed *= g.config.effect.player.slowDown.multiplier,
    tampers: ['p.speed'],
    duration: 400
  },
  partyMode: {
    name: 'Party',
    onStart: function (p, g, isTamperd) {
      this.count = 0
      this.originalColor = p.color
      this.workWithRealColor = !isTamperd('p.color')
    },
    onTick: function (p, g) {
      if (this.count === 0) {
        if (this.workWithRealColor) {
          p.color = getRandomColor()
          g.uiManager.updateRanking()
        }
        p.drawColor = getRandomColor()
        this.count = 30
      }
      this.count--
    },
    onEnd: function (p, g) {
      if (this.workWithRealColor) {
        p.color = this.originalColor
        g.uiManager.updateRanking()
      }
      p.drawColor = p.color
    },
    tampers: ['p.drawColor', 'p.color'],
    duration: 400
  },
  phaseWorld: {
    name: 'Phase World',
    onStart: (p) => {
      p.collision.world = false
    },
    tampers: [
      'p.collision.world'
    ],
    duration: 400,
    onEnd: (p, game, isTamperd) => {
      if (!isTamperd('p.collision.world')) {
        p.collision.world = true
      }
    }
  },
  switchKeys: {
    name: 'Switched',
    onStart: (p) => {
      p.controls.switched = true
    },
    tampers: ['p.controls.switched'],
    duration: 400,
    onEnd: (p, g, isTamperd) => {
      if (!isTamperd('p.controls.switched')) {
        p.controls.switched = false
      }
    }
  },
  fullForce: {
    name: 'Full Force Ahead',
    onStart: (p) => {
      p.controls.disabled = true
    },
    tampers: ['p.controls.disabled'],
    duration: 150,
    onEnd: (p, g, isTamperd) => {
      if (!isTamperd('p.controls.disabled')) {
        p.controls.disabled = false
      }
    }
  },
  shielded: {
    name: 'Shielded',
    onStart: (p) => {
      p.shielded = true
    },
    tampers: ['p.shielded'],
    duration: 400,
    onEnd: (p, g, isTamperd) => {
      if (!isTamperd('p.shielded')) {
        p.shielded = false
      }
    }
  },
  ice: {
    name: 'Ice',
    onStart: (p, g) => {
      p.driftFactor += g.config.effect.player.ice.driftFactor
    },
    tampers: ['p.driftFactor'],
    duration: 200,
    onEnd: (p, g) => {
      p.driftFactor -= g.config.effect.player.ice.driftFactor
    }
  },
  moonfish: {
    name: 'Ice',
    onStart: (p, g) => {
      p.moonfishFactor += g.config.effect.player.moonfish.moonfishFactor
    },
    tampers: ['p.moonfishFactor'],
    duration: 200,
    onEnd: (p, g) => {
      p.moonfishFactor -= g.config.effect.player.moonfish.moonfishFactor
    }
  }
}

const gameEffectClasses = {
  phaseWorld: {
    name: 'Phase World',
    onStart: (g) => {
      g.collision.world = false
    },
    tampers: [
      'g.collision.world'
    ],
    duration: 400,
    onEnd: (g, isTamperd) => {
      if (!isTamperd('g.collision.world')) {
        g.collision.world = true
      }
    }
  },
  portal: {
    name: 'Portal',
    onStart: function (g) {
      this.id = g.spawnPortal()
    },
    duration: 1000,
    onEnd: function (g) {
      g.portals = g.portals.filter(p => p.id !== this.id)
    }
  },
  cloud: {
    name: 'Cloud',
    onStart: (g) => {
      g.cloudMode = true
    },
    tampers: ['g.cloudMode'],
    duration: 400,
    onEnd: (g, isTamperd) => {
      if (!isTamperd('g.cloudMode')) {
        g.cloudMode = false
      }
    }
  }
}

function createEffect (effect, duration) {
  let ret = {
    name: '',
    onStart: (p, game) => {},
    onTick: (p, game) => {},
    onEnd: (p, game) => {},
    tampers: [],
    duration: 0,
    ...effect,
    durationCounter: 0
  }
  if (typeof duration !== 'undefined') {
    ret.duration = duration
  }
  return ret
}