const notDead = p => !p.isDead
const notShielded = p => !p.shielded
const ITEMCLASSES = [
  {
    name: 'speedup-me',
    image: loadImage('images/speedup-me.svg'),
    description: 'Doubles the speed of the current player (applies "Speedup" effect)',
    onHit: (p, gamestate) => {
      gamestate.addPlayerEffect(p, 'speedUp')
    }
  },
  {
    name: 'speedup-other',
    image: loadImage('images/speedup-other.svg'),
    description: 'Doubles the speed of all other players (applies "Speedup" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(po => {
          gamestate.addPlayerEffect(po, 'speedUp')
        })
    }
  },
  {
    name: 'slowdown-me',
    image: loadImage('images/slowdown-me.svg'),
    description: 'Halfs the speed of the current player (applies "Slowdown" effect)',
    onHit: (p, gamestate) => {
      gamestate.addPlayerEffect(p, 'slowDown')
    }
  },
  {
    name: 'slowdown-other',
    image: loadImage('images/slowdown-other.svg'),
    description: 'Halfs the speed of all other players (applies "Slowdown" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(po => {
          gamestate.addPlayerEffect(po, 'slowDown')
        })
    }
  },
  {
    name: 'sizedown-me',
    image: loadImage('images/sizedown-me.svg'),
    description: 'Reduces the size of the current player (applies "Sizedown" effect)',
    onHit: (p, gamestate) => {
      gamestate.addPlayerEffect(p, 'sizeDown')
    }
  },
  {
    name: 'sizeup-other',
    image: loadImage('images/sizeup-other.svg'),
    description: 'Increases the size of all other players (applies "Sizeup" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(po => {
          gamestate.addPlayerEffect(po, 'sizeUp')
        })
    }
  },
  {
    name: 'clear-all',
    image: loadImage('images/clear-all.svg'),
    description: 'Clears all player paths',
    onHit: (p, gamestate) => {
      gamestate.gMove.clear()
    }
  },
  {
    name: 'clear-circle',
    image: loadImage('images/clear-circle.svg'),
    description: 'Clears all player paths around the current player',
    onHit: (p, gamestate) => {
      const SIZE_MULTIPLIER = 40
      let old = gamestate.gMove.fillStyle
      gamestate.gMove.beginPath()
      gamestate.gMove.arc(p.pos.x, p.pos.y, p.size * SIZE_MULTIPLIER, 0, 2 * Math.PI, false)
      gamestate.gMove.fillStyle = '#000000'
      gamestate.gMove.fill()
      gamestate.gMove.fillStyle = old
    }
  },
  {
    name: '90deg-me',
    image: loadImage('images/90deg-me.svg'),
    description: 'Lets the current player turn in 90 degree turns (applies "Turn 90" effect)',
    onHit: (p, gamestate) => {
      gamestate.addPlayerEffect(p, 'turn90')
    }
  },
  {
    name: '90deg-other',
    image: loadImage('images/90deg-other.svg'),
    description: 'Lets all other players turn in 90 degree turns (applies "Turn 90" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(po => gamestate.addPlayerEffect(po, 'turn90'))
    }
  },
  {
    name: 'random',
    image: loadImage('images/random.svg'),
    description: 'Chooses a random item to be issued',
    onHit: (p, gamestate) => {
      gamestate.usedItems[Math.floor(Math.random() * gamestate.usedItems.length)].onHit(p, gamestate)
    }
  },
  {
    name: 'ghost-me',
    image: loadImage('images/ghost-me.svg'),
    description: 'Current player gets to be a ghost (no collision, no trail) (applies "Ghost" effect)',
    onHit: (p, gamestate) => {
      gamestate.addPlayerEffect(p, 'ghost')
    }
  },
  {
    name: 'ghost-other',
    image: loadImage('images/ghost-other.svg'),
    description: 'All other players get to be ghosts (no collision, no trail) (applies "Ghost" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(p => {
          gamestate.addPlayerEffect(p, 'ghost')
        })
    }
  },
  {
    name: 'partymode',
    image: loadImage('images/partymode.svg'),
    description: 'Its a party! (applies "party mode" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .forEach(p => {
          gamestate.addPlayerEffect(p, 'partyMode')
        })
    }
  },
  {
    name: 'phase-world-me',
    image: loadImage('images/phase-world-me.svg'),
    description: 'Current player can walk freely through the world (not player paths though) (applies "Phase World" effect)',
    onHit: (p, gamestate) => {
      gamestate.addPlayerEffect(p, 'phaseWorld')
    }
  },
  {
    name: 'phase-world-all',
    image: loadImage('images/phase-world-all.svg'),
    description: 'Everyone walks freely through the world (not player paths though) (applies "Phase World" effect)',
    onHit: (p, gamestate) => {
      gamestate.addGameEffect('phaseWorld')
    }
  },
  {
    name: 'switch-keys-other',
    image: loadImage('images/switch-keys-other.svg'),
    description: 'Switches the keys of other players (applies "Switch Keys" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(p => {
          gamestate.addPlayerEffect(p, 'switchKeys')
        })
    }
  },
  {
    name: 'spawn-new-items',
    image: loadImage('images/spawn-new-items.svg'),
    description: 'Spawn 3 new items',
    onHit: (p, gamestate) => {
      for (let i = 0; i < 3; ++i) {
        setTimeout(() => gamestate.spawnRandomItem(), 250 * i)
      }
    }
  },
  {
    name: 'switch-positions-all',
    image: loadImage('images/switch-positions-all.svg'),
    description: 'Exchanges the position with other players randomly once',
    onHit: (p, gamestate) => {
      let players = gamestate.players
        .filter(notDead)
        .filter(notShielded)
      if (players < 2) {
        return
      }
      let positions = players.map(p => ({ pos: p.pos, direction: p.direction }))
      let oldpos = [...positions]
      while (arrayHasEqual(positions, oldpos)) {
        shuffle(positions)
      }
      players.forEach((p, i) => {
        p.pos = positions[i].pos
        p.direction = positions[i].direction
      })
    }
  },
  {
    name: 'full-force-ahead-other',
    image: loadImage('images/fullforce-ahead-other.svg'),
    description: 'Full force ahead! No turning! (for others) (applies "Full Force" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(po => po !== p)
        .filter(notDead)
        .filter(notShielded)
        .forEach(p => {
          gamestate.addPlayerEffect(p, 'fullForce')
        })
    }
  },
  {
    name: 'full-force-ahead-all',
    image: loadImage('images/fullforce-ahead-all.svg'),
    description: 'Full force ahead! No turning! (applies "Full Force" effect)',
    onHit: (p, gamestate) => {
      gamestate.players
        .filter(notDead)
        .filter(notShielded)
        .forEach(p => {
          gamestate.addPlayerEffect(p, 'fullForce')
        })
    }
  },
  {
    name: 'portal',
    image: loadImage('images/portal.svg'),
    description: 'Teleport from one place to another',
    onHit: (p, g) => {
      g.addGameEffect('portal')
    }
  },
  {
    name: 'shield',
    image: loadImage('images/shield-me.svg'),
    description: 'Stops others from attacking you (red items)',
    onHit: (p, g) => {
      g.addPlayerEffect(p, 'shielded')
    }
  },
  {
    name: 'cloud',
    image: loadImage('images/cloud-all.svg'),
    description: 'Everyone will have problems to see the map',
    onHit: (p, g) => {
      g.addGameEffect('cloud')
    }
  },
  {
    name: 'ice-all',
    image: loadImage('images/ice-all.svg'),
    description: 'It is going to get slippery',
    onHit: (p, g) => {
      g.players
        .filter(notDead)
        .filter(notShielded)
        .forEach(p => {
          g.addPlayerEffect(p, 'ice')
        })
    }
  },
  {
    name: 'bots',
    image: loadImage('images/bots.svg'),
    description: 'Bots will join the game',
    onHit: (p, g) => {
      const player = {
        id: 100 + g.players.length,
        name: 'Bot #' + (g.players.filter(p => p.bot).length + 1).toString(),
        color: getRandomColor(),
        controls: {},
        points: 0,
        bot: function (p, g) {
          let newDirection = p.direction

          let leftCount = 0
          let rightCount = 0
          // bot should check an area of pixels before his head (where he can move) and then decide to go where less blocked pixels are
          for (let i = 0; i < 5; ++i) {
            for (let j = 0; j < i + 1; ++j) {
              // check if pixel is blocked
              let angleOff = (-1 * i * i * j / 50 / 8 + 120) * 2 * Math.PI
              const cx = p.pos.x + Math.cos(p.direction - angleOff) * (j * 14 + 3)
              const cy = p.pos.y + Math.sin(p.direction - angleOff) * (j * 14 + 3)

              if (cx < 0 || cy < 0 || cx >= g.movementCanvas.width || cy >= g.movementCanvas.height) {
                leftCount++
              } else {
                let cd = g.gMove.getImageData(cx, cy, 1, 1).data
                if (cd[0] + cd[1] + cd[2] !== 0) {
                  leftCount++
                } else {
                  let cd = g.gWorld.getImageData(cx, cy, 1, 1).data
                  if (cd[0] + cd[1] + cd[2] !== 0) {
                    leftCount++
                  }
                }
              }

              const cx2 = p.pos.x + Math.cos(p.direction + angleOff) * (j * 4 + 1)
              const cy2 = p.pos.y + Math.sin(p.direction + angleOff) * (j * 4 + 1)
              if (cx2 < 0 || cy2 < 0 || cx2 >= g.movementCanvas.width || cy2 >= g.movementCanvas.height) {
                rightCount++
              } else {
                let cd2 = g.gMove.getImageData(cx2, cy2, 1, 1).data

                if (cd2[0] + cd2[1] + cd2[2] !== 0) {
                  rightCount++
                } else {
                  let cd2 = g.gWorld.getImageData(cx2, cy2, 1, 1).data
                  if (cd2[0] + cd2[1] + cd2[2] !== 0) {
                    rightCount++
                  }
                }
              }
            }
          }

          if (leftCount > rightCount) {
            newDirection = p.direction - g.config.player.turnSpeed
          } else if (rightCount > leftCount) {
            newDirection = p.direction + g.config.player.turnSpeed
          }

          return newDirection
        }
      }
      g.addPlayer(player)
    }
  }
]

const defaultUsedItems = {
  'speedup-me': true,
  'speedup-other': true,
  'slowdown-me': true,
  'slowdown-other': true,
  'sizedown-me': true,
  'sizeup-other': true,
  'clear-all': true,
  'clear-circle': true,
  '90deg-me': true,
  '90deg-other': true,
  'random': true,
  'ghost-me': true,
  'ghost-other': true,
  'partymode': true,
  'phase-world-me': true,
  'phase-world-all': true,
  'switch-keys-other': true,
  'spawn-new-items': true,
  'switch-positions-all': true,
  'full-force-ahead-other': true,
  'full-force-ahead-all': true,
  'portal': true,
  'shield': true,
  'cloud': true,
  'ice-all': true,
  'bots': true,
}

function loadSelectedItemClasses () {
  let urlParams = new URLSearchParams(window.localStorage.getItem('currentGameConfig'))
  return ITEMCLASSES.filter((c) => {
    return urlParams.get('item-' + c.name) === 'on'
  })
}