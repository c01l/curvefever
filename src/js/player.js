function readPlayerStartConfig() {
  let players = []
  let urlParams = new URLSearchParams(window.localStorage.getItem('currentGameConfig'))

  let playerCount = urlParams.get('playerCount')
  for (let i = 0; i < playerCount; ++i) {
    let key = 'player[' + i + ']'

    if (!urlParams.has(key + '[left]') || !urlParams.has(key + '[right]')) {
      console.log('Skipping player ' + i + ' because keys (left, right) are not set')
      continue
    }

    players.push({
      id: i,
      name: urlParams.get(key + '[name]') || 'Player ' + i,
      color: urlParams.has(key + '[color]') ? '#' + trim(urlParams.get(key + '[color]'), '#') : getRandomColor(),
      controls: {
        left: urlParams.get(key + '[left]'),
        right: urlParams.get(key + '[right]')
      },
      points: 0
    })
  }

  return players
}